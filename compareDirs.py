#!/bin/env python3
import sys
import os
import argparse
from glob import glob



def main():
    args=processArgs()

    files1 = set(getFiles(args.dir1))
    files2 = set(getFiles(args.dir2))
    print("IN {} BUT NOT IN {}".format(args.dir1, args.dir2))
    for f in files1 - files2:
        print(f)
    print("\nIN {} BUT NOT IN {}".format(args.dir2, args.dir1))
    for f in files2 - files1:
        print(f)
    

    
    
    
def getFiles(dir):
    files = [os.path.basename(f) for f in glob(os.path.join(dir, '*'))]
    files.sort()

    return files

def processArgs():
    class MyParser(argparse.ArgumentParser):
        def error(self, message):
            sys.stderr.write('\nError: %s\n\n' % message)
            self.print_help()
            sys.exit(2)
    
    class Checkerrors(argparse.Action) :
        def __call__(self,parser,namespace,value,option_string) :
            if (option_string==None) :
                if os.path.isdir(value):
                    setattr(namespace,self.dest,value)
                else :
                    parser.error("The -f flag needs a valid filename")
            
    
    
    
    #argParser = MyParser(usage=("%s (sourceDir & filter) | filterFile" % (os.path.basename(sys.argv[0]))))
    argParser = MyParser(description="""Compare two directories and list any filenames that don't appear in both
                         """)

    argParser.add_argument('dir1', metavar="", action=Checkerrors, help="Directory path 1")
    argParser.add_argument('dir2', metavar="", action=Checkerrors, help="Directory path 2")
    ap=argParser.parse_args()
    return ap




#This is required because by default this is a module.  Running this makes it execute main as if it is a script
if __name__ == '__main__':
    main()
    