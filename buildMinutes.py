#!/usr/bin/env python3

from docx import Document
from docx.shared import Pt, Inches
from collections import OrderedDict, defaultdict
from glob import glob
import os.path
import re
import sys


# TODO: Central Oregon Intergropu formatting looks funny


def main():

    dNumbers = list(range(1, 33))
    dNumbers += range(34, 38)
    dNames = ['District ' + str(d) for d in dNumbers]
    districts = OrderedDict(zip(dNames, dNames))

    officers = OrderedDict([
        ('Delegate', 'Delegate'),
        ('Delegate - Pacific Regional Forum Report', 'Delegate - Forum'),
        ('Delegate - PRAASA Report', 'Delegate - PRAASA'),
        ('Delegate - PNC Report', 'Delegate - PNC'),
        ('Alternate Delegate', 'Alt Delegate'),
        ('Alternate Delegate - Pacific Regional Forum Report', 'Alt Delegate - Forum'),
        ('Alternate Delegate - PRAASA Report', 'Alt Delegate - PRAASA'),
        ('Alternate Delegate - PNC Report', 'Alt Delegate - PNC'),
        ('Chair', 'Chair'),
        ('Chair - Pacific Regional Forum Report', 'Chair - Forum'),
        ('Chair - PRAASA Forum Report', 'Chair - PRAASA'),
        ('Chair - PNC Forum Report', 'Chair - PNC'),
        ('Alternate Chair', 'Alt Chair'),
        ('Alternate Chair - Pacific Regional Forum Report', 'Alt Chair - Forum'),
        ('Alternate Chair - PRAASA Report', 'Alt Chair - PRAASA'),
        ('Alternate Chair - PNC Report', 'Alt Chair - PNC'),
        ('Secretary', 'Secretary'),
        ('Registrar', 'Registrar'),
        ('Treasurer', 'Treasurer'),
        ('Alternate Treasurer', 'Alt Treasurer'),
    ])
    committees = OrderedDict([
        ('Archives', 'Archives'),
        ('Accessibilities', 'Access'),
        ('Cooperation with the Professional Community (CPC)', 'CPC'),
        ('Cooperation with the Professional Community (CPC) - North', 'CPC North'),
        ('Cooperation with the Professional Community (CPC) - South', 'CPC South'),
        ('Corrections', 'Corrections'),
        ('Finance', 'Finance'),
        ('Grapevine', 'Grapevine'),
        ('Hospitals', 'Hospitals'),
        ('Newsletter', 'Newsletter'),
        ('Public Information', 'PI'),
        ('Public Information - North', 'PI North'),
        ('Public Information - South', 'PI South'),
        ('Translation', 'Translation'),
        ('Treatment Facilities', 'TF'),
        ('Treatment Facilities - North', 'TF North'),
        ('Treatment Facilities - South', 'TF South'),
        ('Website', 'Website'),
    ])
    intergroups = OrderedDict([
        ('Central Oregon Intergroup', 'COI'),
        ('Emerald Valley Intergroup', 'EVI'),
        ('Portland Area Intergroup', 'PAI'),
        ('Westside Central Office', 'WCO'),
    ])
    hostCommittees = OrderedDict([
        ('November 2016 - Medford', 'Host Committee Nov 2016'),
        ('February 2017 - Medford', 'Host Committee Feb 2017'),
        ('May 2017 - Medford', 'Host Committee May 2017'),
        ('September 2017 - Medford', 'Host Committee Sep 2017'),
        ('November 2017 - Medford', 'Host Committee Nov 2017'),
        ('February 2018 - Medford', 'Host Committee Feb 2018'),
    ])

    allFileNames = list(districts.values()) +\
        list(officers.values()) +\
        list(committees.values()) +\
        list(intergroups.values()) +\
        list(hostCommittees.values())

    files = glob('./reports/*')
    fileNames = [os.path.basename(f).split('.')[0] for f in files]
    fileDict = dict(zip(fileNames, files))

    # print('orig, mod', filesOrig, filesMod)

    processList(districts, fileDict, 'rpts-districts.docx', True)
    processList(officers, fileDict, 'rpts-officers.docx', False)
    processList(committees, fileDict, 'rpts-committees.docx', True)
    processList(intergroups, fileDict, 'rpts-intergroups.docx', False)
    processList(hostCommittees, fileDict, 'rpts-hostCommittees.docx', False)

    # check to see if there are any filenames that don't match districts

    for file in fileNames:
        if file not in allFileNames:
            print("This file doesn't have a corresponding position: {}".format(file))


def makeODict(alist):
    return OrderedDict(list(zip(alist, [0] * len(alist))))


def processList(positionList, files, docName, titleFlag):
    # print('poslist', positionList)
    # print('files', files)
    doc = Document()
    style = doc.styles['Normal']
    font = style.font
    font.name = 'Arial'
    font.size = Pt(11)
    para = style.paragraph_format
    para.space_after = Pt(0)
    para.line_spacing = 1
    for pName, fileName in positionList.items():
        if fileName in list(files.keys()):
            file = files[fileName]
            p = doc.add_paragraph()
            p.add_run(pName).bold = True
            mergeDocx(file, doc)
            doc.add_paragraph()
            doc.add_paragraph()
        else:
            if titleFlag:
                p = doc.add_paragraph()
                p.add_run(pName).bold = True
                p = doc.add_paragraph("No report provided")
                doc.add_paragraph()
                doc.add_paragraph()


        

    doc.save(docName)


def mergeDocx(source, doc):
    rpt = Document(source)
    bufferFlag = False
    blankLineFlag = False
    #print('++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')
    #print('++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')
    for rptPara in rpt.paragraphs:
        #print('para', rptPara.text)

        '''
        If there are blank lines at beginning of the doc, bufferflag will be 
        false and the blank lines will be completely skipped.  If the buffer
        flag is true, as it will be for the body of the report, multiple blank
        lines will be collapsed into a single blank line.
        '''
        print('text', rptPara.text)
        print('runs')
        for run in rptPara.runs:
            print('text', run.text)

        result = re.search(r'^\s+$', rptPara.text)        
        if rptPara.text == '' or result:
            #print('blnk')
            if bufferFlag:
                #print('buffering')
                blankLineFlag = True

        else:
            print('not blank')
            # print a blank line if one has been indicated and if it's
            # not the first text paragraph in the doc
            if blankLineFlag:
                p = doc.add_paragraph()
                blankLineFlag = False

            if hasParaSpacing:
                blankLineFlag = True

            bufferFlag = True
            
            docPara = doc.add_paragraph(rptPara.text)
            
            # format the various list styles
            if 'List' in rptPara.style.name:
                #print('list')
                docPara.style = doc.styles['List Bullet']
                blankLineFlag = False
            elif rptPara.paragraph_format.first_line_indent:

                if rptPara.paragraph_format.first_line_indent < 0:
                    #print('indent -')
                    docPara.paragraph_format.left_indent = Inches(.5)
                    docPara.paragraph_format.first_line_indent = Inches(-.5)
                    blankLineFlag = False
                    #print(rptPara.paragraph_format.first_line_indent.inches)
                else:
                    #print('indent +')
                    docPara.paragraph_format.first_line_indent = rptPara.paragraph_format.first_line_indent
                    blankLineFlag = False
            elif rptPara.paragraph_format.left_indent:
                docPara.paragraph_format.left_indent = rptPara.paragraph_format.left_indent

                #p.paragraph_format.space_after = Pt(0)

            #print('adding', rptPara.text)
            #docPara.paragraph_format.space_after = Pt(0)

'''
Need to walk up the formatting tree inherited by the document to see if paragraph spacing has 
been applied to the doucment or not.  First check the paragraphs themselves, then check the
document, then the default styles.
'''
def hasParaSpacing(doc, para):
    
    if para.paragraph_format.space_after or \
        para.paragraph_format.space_before:

        return True

    styles = doc.styles
    paraStyleName = para.style.name
    if styles[paraStyleName].paragraph_format.space_after or \
        styles[paraStyleName].paragraph_format.space_before:

        return True

    spacings = styles.xpath('/w:styles/w:docDefaults/w:pPrDefault/w:pPr/w:spacing')
    if spacings[0].get(qn(('w:after'))):
        return True

    return False
            
    
def matchFile(dName, dFiles):
    for dFile in dFiles:
        testName = os.path.basename(dFile).split('.')[0]
        if dName == testName:
            return dFile




if __name__ == "__main__":
    main()