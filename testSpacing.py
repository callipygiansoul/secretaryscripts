#!/usr/bin/env python3

from docx import Document
from docx.shared import Pt, Inches
import os.path
import re
import sys
from docx.oxml.ns import qn  # short for "qualified name"


rpt = Document('./testDoc.docx')

for rptPara in rpt.paragraphs:
    print(rptPara.text)
    print(rptPara.paragraph_format.space_after)
    print(rptPara.paragraph_format.space_before)
    print(rptPara.style.name)
    paraStyleName = rptPara.style.name
    print(rpt.styles[paraStyleName].paragraph_format.space_after)
    print(rpt.styles[paraStyleName].paragraph_format.space_before)
    print(rpt.styles[paraStyleName].base_style)
    style_element = rpt.styles[paraStyleName].element
    styles = rpt.styles.element
    print(style_element.get(qn('w:default')))  # prints a boolean value or None if no such attr present
    print(style_element.xml)
    print('=====================')
    print('styles xlm', rpt.styles.element.xml)
    print('styles spacing', styles.get(qn('w:spacing')))  # prints a boolean value or None if no such attr present

document_elm = rpt.element
spacings = styles.xpath('/w:styles/w:docDefaults/w:pPrDefault/w:pPr/w:spacing')
print(spacings[0].get(qn(('w:after'))))


# for style in rpt.styles:
#     if 'PARAGRAPH' in str(style.type):
#         print('name', style.name, style.paragraph_format.space_after)
#     print('style={}, type=|{}|'.format(style.name, style.type))